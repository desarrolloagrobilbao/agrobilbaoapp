import gql from 'graphql-tag';

export const NUEVO_CLIENTE = gql`
	mutation createPersona($input: PersonaInput!) {
		addPersona(input: $input) {
			id_persona
			numero_documento
			primer_nombre
			segundo_nombre
			primer_apellido
			segundo_apellido
			email
			telefono
		}
	}
`;

export const UPDATE_PERSONA = gql`
	mutation update_persona($input: PersonaInput!) {
		updatePersona(input: $input) {
			id_persona
			tipo_documento {
				id_tipo_documento
				iniciales
				nombre_tipo_documento
			}
			lugar {
				id_lugar
				nombre_lugar
			}
			tipo_persona {
				id_tipo_persona
				nombre_tipo_persona
			}
			numero_documento
			primer_nombre
			segundo_nombre
			primer_apellido
			segundo_apellido
			email
			telefono
		}
	}
`;

export const NEW_ORDER = gql`
	mutation crearPedido($input: PedidoInput!) {
		addPedido(input: $input) {
			id_pedido
			persona {
				id_persona
				primer_nombre
				primer_apellido
			}
			cantidad
			fecha_pedido
			fecha_entrega
			direccion
			descripcion
			tarifa
			valor_flete
		}
	}
`;

export const UPDATE_ORDER = gql`
	mutation updateOrder($input: PedidoInput!) {
		updatePedido(input: $input) {
			id_pedido
			persona {
				id_persona
				primer_nombre
				primer_apellido
			}
			cantidad
			fecha_pedido
			fecha_entrega
			direccion
			descripcion
			tarifa
			valor_flete
		}
	}
`;

export const CREATE_PERSONAXPEDIDO = gql`
	mutation crearPersonaPedido($input: PersonaPedidoInput!) {
		addPersonaXPedido(input: $input) {
			id_personaxpedido
			persona {
				id_persona
				primer_nombre
				primer_apellido
			}
			pedido {
				id_pedido
				cantidad
			}
			cantidad_asociado
			fecha_recepcion
			descripcion_pp
		}
	}
`;

export const UPDATE_PERSONA_PIDIDO = gql`
	mutation updatePersonaPedido($input: PersonaPedidoInput!) {
		updatePersonaXPedido(input: $input) {
			id_personaxpedido
			persona {
				id_persona
				primer_nombre
				primer_apellido
			}
			pedido {
				id_pedido
			}
			cantidad_asociado
			fecha_recepcion
			descripcion_pp
		}
	}
`;
