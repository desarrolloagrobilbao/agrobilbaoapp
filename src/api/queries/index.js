import gql from 'graphql-tag';

export const PERSONAS_QUERY = gql`
	{
		personas {
			id_persona
			tipo_documento {
				nombre_tipo_documento
			}
			lugar {
				nombre_lugar
			}
			tipo_persona {
				nombre_tipo_persona
			}
			numero_documento
			primer_nombre
			primer_apellido
			email
			telefono
		}
	}
`;

export const PERSONA_QUERY = gql`
	query QueryPersona($id: ID) {
		persona(id: $id) {
			id_persona
			tipo_documento {
				id_tipo_documento
			}
			lugar {
				id_lugar
			}
			tipo_persona {
				id_tipo_persona
			}
			numero_documento
			primer_nombre
			segundo_nombre
			primer_apellido
			segundo_apellido
			email
			telefono
		}
	}
`;

export const PEDIDOS_QUERY = gql`
	{
		pedidos {
			id_pedido
			persona {
				id_persona
				primer_nombre
				primer_apellido
			}
			cantidad
			fecha_pedido
			fecha_entrega
			direccion
			descripcion
			tarifa
			valor_flete
		}
	}
`;

export const PEDIDO_QUERY = gql`
	query orderQuery($id: ID) {
		pedido(id: $id) {
			id_pedido
			persona {
				id_persona
				primer_nombre
				primer_apellido
			}
			cantidad
			fecha_pedido
			fecha_entrega
			direccion
			descripcion
			tarifa
			valor_flete
		}
	}
`;

export const CLIENTES_QUERY = gql`
	{
		clientes {
			id_persona
			primer_nombre
			primer_apellido
		}
	}
`;

export const FACTURAS_QUERY = gql`
	query {
		facturas {
			id_factura
			personaxpedido {
				asociado: persona {
					id_persona
					primer_nombre
					primer_apellido
				}
				pedido {
					id_pedido
					cliente: persona {
						id_persona
						primer_nombre
						primer_apellido
						email
					}
					cantidad
					direccion
					fecha_entrega
					tarifa
					valor_flete
				}
				cantidad_asociado
			}
			valor_pedido_persona
		}
	}
`;

export const FACTURA_QUERY = gql`
	query($id: ID) {
		factura(id: $id) {
			id_factura
			personaxpedido {
				id_personaxpedido
				fecha_recepcion
				descripcion_pp
				asociado: persona {
					id_persona
					primer_nombre
					primer_apellido
				}
				pedido {
					id_pedido
					cliente: persona {
						id_persona
						primer_nombre
						primer_apellido
					}
					cantidad
					direccion
					fecha_entrega
					tarifa
					valor_flete
				}
				cantidad_asociado
			}
			valor_pedido_persona
		}
	}
`;

export const PESONASPADIDOS = gql`
	query {
		personas {
			id_persona
			primer_nombre
			primer_apellido
		}
		pedidos {
			id_pedido
			cliente: persona {
				id_persona
				primer_nombre
				primer_apellido
			}
			cantidad
		}
	}
`;

export const Login_Query = gql`
	query login($user: String, $password: String) {
		user(user: $user, password: $password) {
			token
		}
	}
`;
