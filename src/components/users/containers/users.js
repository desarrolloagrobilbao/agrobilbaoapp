import React, { Component } from 'react';
import { View, Text, FlatList, Alert } from 'react-native';
import { ListItem } from 'react-native-elements';
import { Query } from 'react-apollo';
import { PERSONAS_QUERY } from '../../../api/queries';

class Users extends Component {
	static navigationOptions = {
		title: 'Usuarios',
		headerRight: <View />
	};
	keyExtractor = (item) => item.id_persona.toString();
	render() {
		return (
			<View>
				<Query query={PERSONAS_QUERY} pollInterval={1000}>
					{({ loading, error, data, startPolling, stopPolling }) => {
						if (loading) return <Text>Loading...</Text>;
						if (error) console.log(error);
						console.log(data);
						return (
							<FlatList
								data={data.personas}
								keyExtractor={this.keyExtractor}
								ListEmptyComponent={() => <Text>No hay personas</Text>}
								renderItem={({ item }) => (
									<ListItem
										key={item.id_persona}
										title={`${item.primer_nombre} ${item.primer_apellido}`}
										onPress={() => {
											Alert.alert(
												'Detalle',
												`Correo: ${item.email} \nTelefono: ${item.telefono}\nCiudad: ${item
													.lugar.nombre_lugar}`
											);
										}}
										leftAvatar={{
											source: {
												uri:
													'https://st2.depositphotos.com/5266903/8082/v/950/depositphotos_80821502-stock-illustration-user-flat-green-and-gray.jpg'
											},
											title: 'F'
										}}
									/>
								)}
							/>
						);
					}}
				</Query>
			</View>
		);
	}
}

export default Users;
