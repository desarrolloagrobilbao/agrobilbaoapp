import React, { Component } from 'react';
import { View, Text, FlatList, Alert } from 'react-native';
import { ListItem } from 'react-native-elements';
import { Query } from 'react-apollo';
import { PEDIDOS_QUERY } from '../../../api/queries';

class Orders extends Component {
	static navigationOptions = {
		title: 'Pedidos',
		headerRight: <View />
	};
	keyExtractor = (item) => item.id_pedido.toString();
	render() {
		return (
			<View>
				<Query query={PEDIDOS_QUERY} pollInterval={1000}>
					{({ loading, error, data, startPolling, stopPolling }) => {
						if (loading) return <Text>Loading...</Text>;
						if (error) console.log(error);
						// console.log(data);
						return (
							<FlatList
								data={data.pedidos}
								keyExtractor={this.keyExtractor}
								ListEmptyComponent={() => <Text>No hay pedidos</Text>}
								renderItem={({ item }) => (
									<ListItem
										key={item.id_pedido}
										title={`Pedido #${item.id_pedido} | Fecha: `+`${item.fecha_pedido}`.split('T')[0]}
										onPress={() => {
											Alert.alert(
												'Detalle',
												`Cantidad: ${item.cantidad} \nCliente: ${item.persona.primer_nombre} ${item.persona.primer_apellido}\nTarifa: ${item.tarifa}\nFlete: ${item.valor_flete}\nFecha Entrega: `+`${item.fecha_entrega}`.split('T')[0]
											);
										}}
										leftAvatar={{
											source: {
												uri:
												'https://cdn2.vectorstock.com/i/1000x1000/95/31/truck-icon-green-vector-18239531.jpg'
											},
											title: 'P'
										}}
									/>
								)}
							/>
						);
					}}
				</Query>
			</View>
		);
	}
}

export default Orders;
