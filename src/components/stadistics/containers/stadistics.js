import React, { Component } from 'react';
import { View, Text, Dimensions, ScrollView } from 'react-native';
import { LineChart, PieChart, BarChart } from 'react-native-chart-kit';

class Stadistics extends Component {
	static navigationOptions = {
		title: 'Estadisticas',
		headerRight: <View />
	};
	render() {
		const data = {
			labels: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ],
			datasets: [
				{
					data: [ 650, 590, 800, 810, 560, 550, 400 ],
					color: (opacity = 1) => `rgba(134, 65, 244, 0.75)`
				}
			]
		};
		const data2 = [
			{ name: 'Jaime Rios', population: 3000, color: 'black', legendFontColor: '#7F7F7F', legendFontSize: 15 },
			{ name: 'Carlos Villamil', population: 500, color: 'gray', legendFontColor: '#7F7F7F', legendFontSize: 15 },
			{
				name: 'Alberto Carrasquilla',
				population: 1000,
				color: 'rgb(0, 0, 255)',
				legendFontColor: '#7F7F7F',
				legendFontSize: 15
			}
		];

		const screenWidth = Dimensions.get('window').width;
		const chartConfig = {
			backgroundGradientFrom: '#1E2923',
			backgroundGradientTo: '#08130D',
			color: (opacity = 1) => `rgba(26, 255, 146, 0.75)`,
			strokeWidth: 2 // optional, default 3
		};
		return (
			<ScrollView>
				<Text>Bezier Line Chart </Text>
				<LineChart data={data} width={screenWidth} height={220} chartConfig={chartConfig} />
				<PieChart
					data={data2}
					width={screenWidth}
					height={220}
					chartConfig={chartConfig}
					accessor="population"
					backgroundColor="transparent"
					absolute
				/>
				<BarChart
					// style={graphStyle}
					data={data}
					width={screenWidth}
					height={220}
					chartConfig={chartConfig}
				/>
			</ScrollView>
		);
	}
}

export default Stadistics;
