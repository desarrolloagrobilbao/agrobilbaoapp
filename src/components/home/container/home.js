import React, { Component } from 'react';
import { View, Alert, Text, StyleSheet } from 'react-native';
import { ListItem, Header, Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class HomeScreen extends Component {
	static navigationOptions = {
		header: null
	};
	render() {
		const list = [
			{
				name: 'Usuarios',
                component: 'Users',
                avatar_url:
					'https://img.freepik.com/vector-gratis/paquete-avatares-usuario_23-2147502629.jpg?size=338&ext=jpg'
			},
			{
				name: 'Pedidos',
                component: 'Orders',
                avatar_url:
					'https://www.consumiblestpv.com/WebRoot/acens/Shops/consumiblestpv_com/59C5/1EDD/AA4B/4224/D259/7F00/0001/3AED/envios-mundo_m.png'
			},
			{
				name: 'Facturas',
                component: 'Invoices',
                avatar_url: 'https://www.logidados.pt/images/imagens_transportes/pedidos.png'
			},
			{
				name: 'Estadisticas',
                component: 'Stadistics',
                avatar_url:
					'https://previews.123rf.com/images/cidepix/cidepix1608/cidepix160802570/61777067-ilustraci%C3%B3n-de-verdes-estad%C3%ADsticas-bares-aislado-en-un-fondo-blanco.jpg'
			}
		];

		return (
			<View>
				<Header
					leftComponent={
						<Avatar
							rounded
							source={require('../../../../assets/img/aguacate.jpg')}
							size={55}
							onPress={() => console.log('Works!')}
							activeOpacity={0.7}
						/>
					}
					centerComponent={{
						text: 'AgrobilbaoApp',
						style: { color: '#fff', fontSize: 25, fontWeight: 'bold' }
					}}
					containerStyle={{
						backgroundColor: '#1b5e20',
						justifyContent: 'space-around'
					}}
				/>
				<Text style={styles.welcome}>Bienvenido a AgrobilbaoApp! </Text>
				{list.map((l, i) => (
					<ListItem
						key={i}
						title={l.name}
						onPress={() => this.props.navigation.navigate(l.component)}
						leftAvatar={{ source: { uri: l.avatar_url } }}
					/>
				))}
			</View>
		);
	}
}
const styles = StyleSheet.create({
	welcome: {
		fontSize: 24,
		paddingVertical: 20,
		fontWeight: 'bold'
	}
});
