import React, { Component } from 'react';
import { View, Text, FlatList, Alert } from 'react-native';
import { ListItem } from 'react-native-elements';
import { Query } from 'react-apollo';
import { FACTURAS_QUERY } from '../../../api/queries';

class Invoces extends Component {
	static navigationOptions = {
		title: 'Facturas',
		headerRight: <View />
	};
	keyExtractor = (item) => item.id_factura.toString();
	render() {
		return (
			<View>
				<Query query={FACTURAS_QUERY} pollInterval={1000}>
						{({ loading, error, data, startPolling, stopPolling }) => {
							if (loading) return <Text>Loading...</Text>;
							if (error) console.log(error);
							// console.log(data);
							return <FlatList
							data={data.facturas}
							keyExtractor={this.keyExtractor}
							ListEmptyComponent={() => <Text>No hay facturas</Text>}
							renderItem={({ item }) => (
								<ListItem
									key={item.id_factura}
									title={`Factura #${item.id_factura} | Cantidad: `+`${item.personaxpedido.cantidad_asociado}`}
									onPress={() => {
										Alert.alert(
											'Detalle',
											`Asociado: ${item.personaxpedido.asociado.primer_nombre} ${item.personaxpedido.asociado.primer_apellido} \nCliente: ${item.personaxpedido.pedido.cliente.primer_nombre} ${item.personaxpedido.pedido.cliente.primer_apellido}\nValor pedido: ${item.valor_pedido_persona}`
										);
									}}
									leftAvatar={{
										source: {
											uri:
											'https://previews.123rf.com/images/arcady31/arcady311510/arcady31151000003/46532249-invoice-icon.jpg'
										},
										title: 'F'
									}}
								/>
							)}
						/>
						}}
					</Query>
			</View>
		);
	}
}

export default Invoces;
