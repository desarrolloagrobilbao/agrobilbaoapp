import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
	FlatList,
	Image, //, Button
	Alert
} from 'react-native';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { Query } from 'react-apollo';
import { PERSONAS_QUERY } from './src/api/queries';
import { Button, Header, Avatar, ListItem } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import OrdersScreen from './src/components/orders/containers/orders';
import UsersScreen from './src/components/users/containers/users';
import HomeScreen from './src/components/home/container/home';
import InvoicesScreen from './src/components/invoices/containers/invoces';
import StadisticsScreen from './src/components/stadistics/containers/stadistics';
import { createStackNavigator, createAppContainer } from 'react-navigation';

const RootStack = createStackNavigator(
	{
		Home: HomeScreen,
		Users: UsersScreen,
		Orders: OrdersScreen,
		Invoices: InvoicesScreen,
		Stadistics: StadisticsScreen
	},
	{
		initialRouteName: 'Home',
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: '#1b5e20'
			},
			headerTintColor: 'white',
			headerTitleStyle: {
				textAlign: 'center',
				flex: 1
			}
		}
	}
);

const AppContainer = createAppContainer(RootStack);

type Props = {};

const client = new ApolloClient({
	uri: 'http:192.168.1.41:4000/api'
});

export default class App extends Component<Props> {
	render() {
		return (
			<ApolloProvider client={client}>
				<AppContainer />
			</ApolloProvider>

			// <Header
			//  		leftComponent={
			//  			<Avatar
			//  				rounded
			//  				source={require('./assets/img/aguacate.jpg')}
			//  				size={55}
			//  				onPress={() => console.log('Works!')}
			//  				activeOpacity={0.7}
			//  			/>
			//  		}
			//  		centerComponent={{ text: 'AgrobilbaoApp', style: { color: '#fff' } }}
			//  		containerStyle={{
			//  			backgroundColor: '#1b5e20',
			//  			justifyContent: 'space-around'
			//  		}}
			//  	/>
			//  	<Text style={styles.welcome}>Bienvenido a AgrobilbaoApp! </Text>
			//  	 <View style={styles.items}>
			//  			<ListItem
			//  				leftAvatar={{
			//  					title: J,
			//  					source: { require('./assets/img/aguacate.jpg') },
			//  					showEditButton: true
			//  				}}
			//  				title="Hola"
			//  				subtitle="Hola mundo"
			//  				chevron
			//  			/>;

			//  <Avatar
			//  				size={150}
			//  				containerStyle={styles.item}
			//  				source={require('./assets/img/aguacate.jpg')}
			//  				onPress={() => console.log('Works!')}
			//  				 activeOpacity={0.7}
			//  				onPress={() => {
			//  					Alert.alert('Hola que hace.');
			//  				}}
			//  				title="Hola que hce"
			//  			/>
			//  			<Avatar
			//  				containerStyle={styles.item}
			//  				source={require('./assets/img/aguacate.jpg')}
			//  				size={150}
			//  				title=""
			//  				onPress={() => console.log('Works!')}
			//  				activeOpacity={0.7}
			//  			/>
			//  		</View>
			//  		<View style={styles.items}>
			//  			<Avatar
			//  				containerStyle={styles.item}
			//  				source={require('./assets/img/aguacate.jpg')}
			//  				size={150}
			//  				title="LW"
			//  				onPress={() => console.log('Works!')}
			//  				activeOpacity={0.7}
			//  				onPress={() => {
			//  					Alert.alert('Hola que hace.');
			//  				}}
			//  			/>
			//  			<Avatar
			//  				containerStyle={styles.item}
			//  				source={require('./assets/img/aguacate.jpg')}
			//  				size={150}
			//  				title="LW"
			//  				onPress={() => console.log('Works!')}
			//  				activeOpacity={0.7}
			//  			/>
			//  		</View> */}
			//  	{list.map((l, i) => (
			//  		<ListItem
			//  			key={i}
			//  			title={l.name}
			//  			onPress={() => {
			//  				Alert.alert(l.name, l.avatar_url);
			//  			}}
			//  			leftAvatar={{ source: { uri: l.avatar_url } }}
			//  		/>
			//  	))}

			//  	 <Query query={PERSONAS_QUERY} pollInterval={1000}>
			//  		{({ loading, error, data, startPolling, stopPolling }) => {
			//  			if (loading) return <Text>Loading...</Text>;
			//  			if (error) console.log(error);
			//  			 console.log(data);
			//  			return (
			//  				<FlatList
			//  					data={data.personas}
			//  					keyExtractor={this.keyExtractor}
			//  					ListEmptyComponent={() => <Text>No hay personas</Text>}
			//  					renderItem={({ item }) => (
			//  						<Text>
			//  							{item.primer_nombre} {item.primer_apellido}
			//  						</Text>
			//  					)}
			//  				/>
			//  			);
			//  		}}
			//  	</Query>
			//  	 <Button
			//  		onPress={() => {
			//  			Alert.alert('Hola que hace.');
			//  		}}
			//  		title="Learn More"
			//  		color="#841584"
			//  		accessibilityLabel="Learn more about this purple button"
			//  	/>
		);
	}
}
